import React, { Component } from 'react';
import { Navbar, Nav, NavDropdown, Badge } from 'react-bootstrap'
import { NavLink } from "react-router-dom";


class Header extends Component{
  render(){
    return (
      <>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
          <Navbar.Brand href="#home">
            Sistema de tienda en linea 2.0
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <NavLink to="/">Inicio</NavLink>
              <NavDropdown title="Menú" id="collasible-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">
                  Nueva venta
                </NavDropdown.Item>

                <NavDropdown.Item href="#action/3.2">
                  Lista de ventas
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
            <h5>
              <Badge variant="secondary">Martes 23 de Marzo del 2020</Badge>
            </h5>
          </Navbar.Collapse>
        </Navbar>
      </>
    );
  }
}

export default Header;