import React, { Component }  from 'react';
import { Table,Container,Button } from 'react-bootstrap'
import { NavLink } from 'react-router-dom';

class Home extends Component{
  render(){
    return (
      <>
        

        <Container fluid="md">
          
          <Button variant="success" className="mb-3 mt-3 float-right" ><NavLink to="/venta" style={{color: "white", textDecoration: 'none'}}>Nueva venta</NavLink></Button>
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th>Folio venta</th>
                <th>No. Cliente</th>
                <th>Nombre</th>
                <th>Total</th>
                <th>Fecha</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
                <td>@mdo</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
                <td>@mdo</td>
              </tr>
              <tr>
                <td>3</td>
                <td colSpan="2">Larry the Bird</td>
                <td>@twitter</td>
                <td>@mdo</td>
              </tr>
            </tbody>
          </Table>
        </Container>
      </>
    );
  }
}

export default Home;