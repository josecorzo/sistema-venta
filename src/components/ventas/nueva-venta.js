import React, { Component, useState, useEffect } from "react";
import { Table,Button,Container,Alert,Form,Row,Col,Modal } from 'react-bootstrap';

const Venta = () =>  {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
      <>
        <Modal show={show} onHide={handleClose} animation={false}>
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
          <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={handleClose}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>
        <Container fluid="md">
          <Form className="mb-3 mt-3">
            <Form.Row className="align-items-center">
              <Col xs="auto">
                <Form.Label>
                  <h4>Cliente</h4>
                </Form.Label>
              </Col>
              <Col xs="auto">
                <Form.Control
                  className="mb-2 mr-sm-2"
                  id="inlineFormInputName2"
                  placeholder="Nombre cliente"
                />
              </Col>

              <Col xs="auto">
                <Button variant="outline-success" className="mb-2" onClick={handleShow}>
                  Agregar
                </Button>
              </Col>
            </Form.Row>

            <Form.Row className="align-items-center">
              <Col xs="auto">
                <Form.Label>
                  <h4>Articulo</h4>
                </Form.Label>
              </Col>
              <Col xs="auto">
                <Form.Control
                  className="mb-2 mr-sm-2"
                  id="inlineFormInputName2"
                  placeholder="Nombre cliente"
                />
              </Col>

              <Col xs="auto">
                <Button variant="outline-success" className="mb-2" onClick={handleShow}>
                  Agregar
                </Button>
              </Col>
            </Form.Row>
          </Form>
          <hr />
          <Table striped bordered hover size="sm">
            <thead>
              <tr>
                <th>Descripción Articulo</th>
                <th>Marca</th>
                <th>Cantidad</th>
                <th>Precio</th>
                <th>Importe</th>
                <th>Accion</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
                <td>@mdo</td>
                <td>@mdo</td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
                <td>@mdo</td>
                <td>@mdo</td>
              </tr>
              <tr>
                <td>3</td>
                <td>Larry the Bird</td>
                <td>@twitter</td>
                <td>@mdo</td>
                <td>@mdo</td>
                <td>@mdo</td>
              </tr>
            </tbody>
          </Table>
          <hr />
          <Table striped bordered hover size="sm">
            <tbody>
              <tr>
                <td>Importe</td>
                <td>Mark</td>
              </tr>
              <tr>
                <td>IVA</td>
                <td>Jacob</td>
              </tr>
              <tr>
                <td>Total</td>
                <td>@twitter</td>
              </tr>
            </tbody>
          </Table>
        </Container>
      </>
    );
  
}

export default Venta;
