import './App.css';
import Router from './Router.js'

function App() {
  return (
    <div className="App">
      <section className="componente">
        <Router/>
      </section>
    </div>
  );
}

export default App;
