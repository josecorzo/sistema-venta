import React, { Component } from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom'
import Home from './components/home.js'
import Venta from './components/ventas/nueva-venta.js'
import Header from "./components/header.js";


class Router extends Component{
  render(){
    return (
      <BrowserRouter>
        <Header />

        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/venta" component={Venta} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default Router;